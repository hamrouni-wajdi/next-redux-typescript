import ChildOne from "../childOne/ChildOne";
import ChildTwo from "../childTwo/ChildTwo";

export default function Parent() {
  return (
    <div>
      <h1>Parent component</h1>
      <label htmlFor="parenttoChildOne">
        <input
          name="parenttoChildOne"
          type="text"
          placeholder=" parent => childOne"
        />
      </label>
      <label htmlFor="parenttoChildTwo">
        <input
          name="parenttoChildTwo"
          type="text"
          placeholder=" parent => childTwo"
        />
      </label>
      <div>
        <ChildOne fromChildTwo={"lala"} fromParent={"lololo"} />
      </div>
      <div>
        <ChildTwo fromChildOne={"from sibling"} fromParent={"from parent "} />
      </div>
    </div>
  );
}
