type child2Props = {
  fromParent:string,
  fromChildOne:string
}


export default function ChildTwo({ fromParent, fromChildOne }:child2Props) {
  return (
    <div>
      <h1>childTwo component</h1>
      <h2> data from childOne component : {fromChildOne}</h2>
      <h2>data from Parent Component : {fromParent}</h2>
      <label htmlFor="childTwo">
        <input
          name="childTwo"
          type="text"
          placeholder=" Enter data from childTWo"
        />
      </label>
    </div>
  );
}





