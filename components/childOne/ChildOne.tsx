
type childProps = {
  fromParent:string,
  fromChildTwo:string
}

export default function ChildOne({ fromParent, fromChildTwo }:childProps) {
  return (
    <div>
      <h1>childTwo One</h1>
      <h2> data from childTWo component : {fromChildTwo}</h2>
      <h2>data from Parent Component : {fromParent}</h2>
      <label htmlFor="childOne">
        <input
          name="childOne"
          type="text"
          placeholder=" Enter data from child One"
        />
      </label>
    </div>
  );
}
